import static org.junit.Assert.*;

import org.junit.Test;

import StringTraining.StringTraining;


public class UTestStringTraining {

	@Test
	public void test_squeezeCharacters01() throws Exception {
		
		String s = "aaaabbccc";
		//StringTraining.squeezeCharacters(s);
		assertEquals("Wrong compressed String","abc", StringTraining.squeezeCharacters(s));
		
	}

}
