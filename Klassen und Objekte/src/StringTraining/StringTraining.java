package StringTraining;
import java.lang.String;

public class StringTraining {
	
	//Konstruktor
	public StringTraining() {
		
	}
	
	//Methoden
	static void enumerateStringChars(String s) {
		//Methode zum ausdrucken der einzelnen Zeichen
		for(int i=0; i < s.length(); i++) {
			System.out.println(i + " " + s.charAt(i) );
			}
		}
	
	static int countOccurrences(String s, char c) {
		//Wie oft c in s vorkommt
		int z�hler = 0;
		for(int i=0; i < s.length(); i++) {
			if(s.charAt(i) == c) {
				z�hler++;
			}
		}
		return z�hler;
		
	}
	
	static int countLongestSequence(String s) {
		//Sequenz aufeinanderfolgender identischer Zeichen
		int vorher = 0;
		int anzahl = 1;
		
		for(int i=0; i<s.length(); i++) {
			anzahl = 1; // anzahl zur�cksetzten auf Wert 1
			while(i+1 < s.length() && s.charAt(i) == s.charAt(i+1)) {
				anzahl++;
				i++;
			}
			if(vorher>anzahl && vorher != 0) {
				anzahl = vorher;
			}
			vorher = anzahl;
			}
		return anzahl;
	}
	
	public static String squeezeCharacters(String s) {
		//komprimiert sequenzen identischer Zeichen zu einem einzigen
		String neu = "";
		for(int i = 0; i < s.length(); i++) {
			while(i+1 < s.length() && s.charAt(i) == s.charAt(i+1)) {
				i++;
			}
			neu = neu + s.charAt(i);
		
	}
		return neu;
	}

}
