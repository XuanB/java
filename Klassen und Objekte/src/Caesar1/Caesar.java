package Caesar1;

public class Caesar {
	//Attribute
	private int s; //Versatz zum Verschieben
	private String alphabet = "abcdefghijklmnopqrstuvwxyz"; // 26 Buchstaben mit indey 0-25
	private String geheimalphabet;
	
	
	//Konstruktoren
	public Caesar(int s) {
		this.s=s;
		s=s-1; //da indexpos im String
	}
	
	//Methoden
	int banzahl = alphabet.length();
	public void changeAlphabet() {
		geheimalphabet = alphabet.substring(s, banzahl-1);
		geheimalphabet = geheimalphabet + alphabet.substring(0, s);
		
	}
	
	public String encrypt(String clear) {
		//indexOf
		int pos;
		String geheimtext = "";
		
		for(int i=0; i < clear.length(); i++) {
		
		pos = alphabet.indexOf(clear.charAt(i));
		geheimtext = geheimtext + geheimalphabet.charAt(pos);
		}
		
		return geheimtext;
	}

	public Object decrypt(String encrypted) {
		int pos;
		String klartext = "";
		
		for(int i=0; i < encrypted.length(); i++) {
		
		pos = geheimalphabet.indexOf(encrypted.charAt(i));
		klartext = klartext + alphabet.charAt(pos);
		}
		
		return klartext;
		
		
	}
	
	//Getter
	public String getGeheimalphabet() {
		changeAlphabet();
		return geheimalphabet;
	}
	
	
	
	

	
	
	

}
