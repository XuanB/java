package Circle;
import java.lang.Math;
public class Circle {

//	Attribute so
	private double cx, cy, radius;
	
	//Konstruktor
	public Circle(double cx, double cy, double radius) {
		this.cx = cx;
		this.cy = cy;
		this.radius = radius;
	}
	
	//Methoden
	public double computeArea() {
		return radius*radius*Math.PI;
		
	}
	
	public double computeDistanceOfCenters(Circle that) {
		double a, b;
		if(this.cy==that.cy) {
			return cx;
		}
		if(this.cx== that.cx) {
			return cy;
		}
		else {
		a = this.cx - that.cx;
		b = this.cy - that.cy;
		
		return Math.sqrt((a*a)+(b*b)); // Zieht die Wurzel eines Wertes
		}
	}
	
	public boolean touches(Circle that) {
		if(this.radius+that.radius >= computeDistanceOfCenters(that)) {
			return true;
		}
		else {
		return false;
		}
		
	}

}
