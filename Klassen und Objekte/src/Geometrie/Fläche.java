package Geometrie;
public class Fl�che {
	
	//Attribute
	public int a, b, c;
	public String fl�chenname;
	public int umfang;
	public int fl�cheninhalt;
	public boolean dreieck = false, quadrat = false, rechteck = false;
	private static int z�hler = 0; // static um z�hler bei neuem Objektanlegen nicht 0 zu setzen!
	
	//Konstruktor
	public Fl�che() {
		
	}
	
	public Fl�che(int a, int b, int c) {
		this.a=a;
		this.b=b;
		this.c=c;
		dreieck = true;
		fl�chenname = "Dreieck:";
		z�hler++;
	}
	
	public Fl�che(int a, int b) {
		this.a=a;
		this.b=b;
		rechteck = true;
		fl�chenname = "Rechteck:";
		z�hler++;
	}
	
	public Fl�che(int a) {
		this.a=a;
		quadrat = true;
		fl�chenname = "Quadrat:";
		z�hler++;
	}
	
	//Methoden
	
	public void berechneUmfang() {
		//Quadrat
		if(a != 0 && b == 0 && c == 0) {
			umfang=a*4;
		}
		//Rechteck
		if(a != 0 && b != 0 && c == 0) {
			umfang = 2*(a+b);
		}
		//Dreieck
		if(a != 0 && b != 0 && c != 0) {
			umfang = a+b+c;
		}
	}
	
	public void berechneFl�cheninhalt() {
		//Quadrat
		if(a != 0 && b == 0 && c == 0) {
			fl�cheninhalt=a*a;
		}
		//Rechteck
		if(a != 0 && b != 0 && c == 0) {
			fl�cheninhalt = a*b;
		}
		//Dreieck
		if(a != 0 && b != 0 && c != 0) {
			fl�cheninhalt = 0000000000;
		}
	
	}
	
	//Setter (Um im sp�teren Verlauf ein Attribut einem neuen Wert zuzuweisen)
	public void setSeitenl�nge(int a) {
		this.a = a;
	}
	
	//Getter
	public int getUmfang(){
		berechneUmfang();
		return umfang;
	}
	
	public String getFl�chenname() {
		return fl�chenname;
	}
	
	public int getFl�cheinhalt() {
		berechneFl�cheninhalt();
		return fl�cheninhalt;
	}
	
	public int getSeitenl�nge() {
		return a;
	}
	
	public int getZ�hler() {
		return z�hler;
	}
}
