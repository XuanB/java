package Geometrie;

public class Main {

	public static void main(String[] args) {
	
		Fl�che q1 = new Fl�che(6);
		System.out.println(q1.getFl�chenname());
		System.out.println("Umfang = " + q1.getUmfang());
		System.out.println("Fl�che = " + q1.getFl�cheinhalt());
		System.out.println("");
		
		Fl�che q2 = new Fl�che(6,5);
		System.out.println(q2.getFl�chenname());
		System.out.println("Umfang = " + q2.getUmfang());
		System.out.println("Fl�che = " + q2.getFl�cheinhalt());
		System.out.println("");
		
		Fl�che q3 = new Fl�che(6,5,7);
		System.out.println(q3.getFl�chenname());
		System.out.println("Umfang = " + q3.getUmfang());
		System.out.println("Fl�che = " + q3.getFl�cheinhalt());
		System.out.println("");
		
		System.out.println(q3.getZ�hler());
		

	}

}
